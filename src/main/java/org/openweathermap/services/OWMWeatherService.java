package org.openweathermap.services;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.MediaType;

import org.apache.cxf.jaxrs.client.WebClient;
import org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider;
import org.openweathermap.models.Error;
import org.openweathermap.models.factories.OWMResponseFactory;

import com.wks.weather.models.ExternalServiceResponseObject;
import com.wks.weather.services.IExternalWeatherService;

/**
 * An implementation of {@link com.wks.weather.services.IExternalWeatherService
 * IExternalWeatherService} for OpenWeatherMap's Weather Service.
 * 
 * @author Waqqas
 * 
 */
public class OWMWeatherService implements IExternalWeatherService
{
    private static final String API_BASE_ADDRESS = "http://api.openweathermap.org/data/2.5";
    private static final String PATH_CURRENT_WEATHER_SERVICE = "/weather";
    private static final String PARAM_QUERY = "q";
    private static final String PARAM_UNITS = "units";

    /**
     * Sends a request to OpenWeatherMap's weather service and fetches current weahter information for given city, expressed in given units.
     * @param aCity name of the city.
     * @param aUnit type of unit (e.g. Metric, Imperial).
     * @return an implementation of {@link com.wks.weather.models.ExternalServiceResponseObject ExternalServiceResponseObject}.<br/>
     * If request is successful, this will be an instance of {@link org.openweathermap.models.WeatherInfo WeatherInfo}.<br/>
     * If request failed, this will be an instance of {@link org.openweathermap.models.Error Error}.
     */
    public ExternalServiceResponseObject getCurrentWeather(String aCity, String aUnit) throws Exception
    {
	if(aCity == null || aCity.trim().isEmpty()){
	    return new Error("500","City name not provided.");
	}

	List<Object> providers = new ArrayList<Object>();
	providers.add(new JacksonJaxbJsonProvider());

	WebClient client = WebClient.create(API_BASE_ADDRESS, providers);
	client = client.accept(MediaType.APPLICATION_JSON).type(MediaType.APPLICATION_JSON)
		.path(PATH_CURRENT_WEATHER_SERVICE).query(PARAM_QUERY, aCity).query(PARAM_UNITS, aUnit);

	String resp = client.get(String.class);
	System.out.println(resp);
	return ((ExternalServiceResponseObject) new OWMResponseFactory().fromJSON(resp));
    }

}
