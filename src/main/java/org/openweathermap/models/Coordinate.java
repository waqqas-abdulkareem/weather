package org.openweathermap.models;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * A model class used to store the coordinates of a city.
 * @author Waqqas
 *
 */
public class Coordinate implements OWMModel
{
    private static final String JSON_LATITUDE = "lat";
    private static final String JSON_LONGITUDE = "lon";
    
    /**
     * latitude of the city.
     */
    @JsonProperty(JSON_LATITUDE)
    private float latitude;
    /**
     * longitude of the city.
     */
    @JsonProperty(JSON_LONGITUDE)
    private float longitude;
    
    public Coordinate()
    {
	// TODO Auto-generated constructor stub
    }
    
    /**
     * constructor
     * @param aLongitude longitude of city.
     * @param aLatitude latitude of city.
     */
    public Coordinate(float aLatitude,float aLongitude){
	this.latitude = aLatitude;
	this.longitude = aLongitude;
    }
    
    /**
     * 
     * @return latitude of the city.
     */
    public float getLatitude()
    {
	return latitude;
    }
    
    /**
     * 
     * @return longitude of the city.
     */
    public float getLongitude()
    {
	return longitude;
    }
    
    
    @Override
    public String toString()
    {
	return String.format("Coordinate[lat: %f,lon: %f]", this.getLatitude(),this.getLongitude());
    }
}
