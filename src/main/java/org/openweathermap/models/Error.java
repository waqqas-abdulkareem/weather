package org.openweathermap.models;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * A model class that stores details of an error encountered by the OpenWeatherMap service.
 * @author Waqqas
 *
 */
public class Error implements OWMResponseObject
{	
    private static final String JSON_MESSAGE = "message";
    private static final String JSON_CODE = "cod";
    
    /**
     * Description of the error.
     */
    @JsonProperty(JSON_MESSAGE)
    private String messgae;
    /**
     * Error Code.
     */
    @JsonProperty(JSON_CODE)
    private String code;
    
    public Error()
    {
	// TODO Auto-generated constructor stub
    }
    
    /**
     * Constructor
     * @param aCode error code.
     * @param aMessage description of error.
     */
    public Error(String aCode,String aMessage){
	this.messgae = aMessage;
	this.code = aCode;
    }
    
    /**
     * 
     * @return description of the error.
     */
    public String getMessgae()
    {
	return messgae;
    }
    
    /**
     * 
     * @return error code.
     */
    public String getCode()
    {
	return code;
    }
    
    @Override
    public String toString()
    {
        return String.format("Error[message: %s,code: %s]",  this.getMessgae(),this.getCode());
    }
}
