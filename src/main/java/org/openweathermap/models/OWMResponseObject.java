package org.openweathermap.models;

import com.wks.weather.models.ExternalServiceResponseObject;

/**
 * an interface that must be implemented by any object that is sent as a response by OpenWeatherMap web service.
 * @author Waqqas
 *
 */
public interface OWMResponseObject extends OWMModel, ExternalServiceResponseObject
{

}
