package org.openweathermap.models.factories;

import org.codehaus.jackson.map.ObjectMapper;
import org.openweathermap.models.WeatherInfo;

import com.wks.utils.encoding.JsonUtils;

/**
 * Produces an instance of {@link org.openweathermap.models.WeatherInfo WeatherInfo}
 * @author Waqqas
 *
 */
public class WeatherInfoFactory implements ModelFactory<WeatherInfo>
{

    /**
     * produces an instance of {@link org.openweathermap.models.WeatherInfo WeatherInfo} from a JSON string.
     * @param json a JSON string.
     * @return an instance of {@link org.openweathermap.models.WeatherInfo WeatherInfo}
     * @throws Exception if there was an error while parsing the JSON.
     */
    public WeatherInfo fromJSON(String json) throws Exception
    {
	ObjectMapper mapper = JsonUtils.getObjectMapper();
	return mapper.readValue(json, WeatherInfo.class);
    }

}
