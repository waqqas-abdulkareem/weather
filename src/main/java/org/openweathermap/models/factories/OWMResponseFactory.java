package org.openweathermap.models.factories;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.openweathermap.models.OWMResponseObject;

import com.wks.utils.encoding.JsonUtils;

/**
 * Factory class that produces an {@link org.openweathermap.models.OWMResponseObject OWMResponseObject}.
 * @author Waqqas
 *
 */
public class OWMResponseFactory implements ModelFactory<OWMResponseObject>
{
    private static final String JSON_KEY_STATUS_CODE = "cod";
    private static final int STATUS_CODE_OK = 200;
    
    /**
     * produces an implementation of {@link org.openweathermap.models.OWMResponseObject OWMResponseObject} from a JSON string.
     * @json json a JSON string.
     * @return an implementation of {@link org.openweathermap.models.OWMResponseObject OWMResponseObject}
     * @throws Exception if there was an error while parsing json.
     */
    public OWMResponseObject fromJSON(String json) throws Exception
    {
	try
	{
	    ObjectMapper mapper = JsonUtils.getObjectMapper();
	    JsonNode rootNode = mapper.readTree(json);
	    JsonNode statusCodeNode = rootNode.path(JSON_KEY_STATUS_CODE);
	    final int status = statusCodeNode.getIntValue();
	    final boolean isError = status != STATUS_CODE_OK;
	    if(isError){
	        return new ErrorFactory().fromJSON(json);
	    }else{
	        return new WeatherInfoFactory().fromJSON(json);
	    }
	}catch (Exception e)
	{
	    e.printStackTrace();
	    throw new Exception("Failure to parse response from Weather Service");
	}
    }
}
