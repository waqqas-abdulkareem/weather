package org.openweathermap.models.factories;

import org.codehaus.jackson.map.ObjectMapper;
import org.openweathermap.models.Error;

import com.wks.utils.encoding.JsonUtils;

/**
 * Produces an {@link org.openweathermap.models.Error Error} object from various sources.
 * @author Waqqas
 *
 */
public class ErrorFactory implements ModelFactory<Error>
{
    /**
     * produces an instance of {@link org.openweathermap.models.Error Error} from a JSON string.
     * @param json a JSON string.
     * @return an instance of <code>Error</code>
     * @throws Exception if there was an error while parsing the JSON.
     */
    public Error fromJSON(String json) throws Exception
    {
	
	ObjectMapper mapper = JsonUtils.getObjectMapper();
	return mapper.readValue(json, Error.class);
    }

}
