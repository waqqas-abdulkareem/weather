package org.openweathermap.models.factories;

import org.openweathermap.models.OWMModel;

/**
 * Interface that must be implemented by all {@link org.openweathermap.models.OWMModel OpenWeatherMap Model} Factories.
 * @author Waqqas
 *
 * @param <T> an implementation of {@link org.openweathermap.models.OWMModel OWMModel}
 */
public interface ModelFactory<T extends OWMModel>
{
    
    public T fromJSON(String json) throws Exception;
}
