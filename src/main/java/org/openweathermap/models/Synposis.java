package org.openweathermap.models;

import java.util.Date;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonDeserialize;

import com.wks.utils.encoding.JsonUtils;

/**
 * Model Object for storing details about the city whose weather information is being recorded.
 * @author Waqqas
 *
 */
public class Synposis implements OWMModel
{
    private static final String JSON_COUNTRY = "country";
    private static final String JSON_SUNRISE = "sunrise";
    private static final String JSON_SUNSET = "sunset";
    
    /**
     * name of the country the city is located in.
     */
    @JsonProperty(JSON_COUNTRY)
    private String country;
    
    /**
     * time of sunrise in city.
     */
    @JsonProperty(JSON_SUNRISE)
    @JsonDeserialize(using=JsonUtils.TimeInSecondsDateDeserializer.class)
    private Date sunrise;
    
    /**
     * time of sunset in city.
     */
    @JsonProperty(JSON_SUNSET)
    @JsonDeserialize(using=JsonUtils.TimeInSecondsDateDeserializer.class)
    private Date sunset;
    
    public Synposis()
    {
	// TODO Auto-generated constructor stub
    }
    
    /**
     * constructor 
     * @param aCountry name of country where city is located.
     * @param aSunriseTime sunset time in city
     * @param aSunsetTime sunrise time in city
     */
    public Synposis(String aCountry,Date aSunriseTime, Date aSunsetTime){
	this.country = aCountry;
	this.sunrise = aSunriseTime;
	this.sunset = aSunsetTime;
    }
    
    /**
     * 
     * @return name of country
     */
    public String getCountry()
    {
	return country;
    }
    
    /**
     * 
     * @return sunrise time in city
     */
    public Date getSunrise()
    {
	return sunrise;
    }
    
    /**
     * 
     * @return sunset time in city.
     */
    public Date getSunset()
    {
	return sunset;
    }
    
    @Override
    public String toString()
    {
        return String.format("Synopsis[country: %s,sunrise: %s, sunset: %s]", this.getCountry(),this.getSunrise(),this.getSunset());
    }
}
