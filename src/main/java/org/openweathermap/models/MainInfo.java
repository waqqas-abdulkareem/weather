package org.openweathermap.models;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Model class  that stores main details about the weather e.g.<br/>
 * Temperature (Current, Minimum, Maximum)<br/>
 * 	 
 * @author Waqqas
 *
 */
public class MainInfo implements OWMModel
{
    private static final String JSON_CURRENT_TEMP = "temp";
    private static final String JSON_MIN_TEMP = "temp_min";
    private static final String JSON_MAX_TEMP = "temp_max";
    
    /**
     * current temperature
     */
    @JsonProperty(JSON_CURRENT_TEMP)
    private float currentTemperature;
    
    /**
     * minimum temperature
     */
    @JsonProperty(JSON_MIN_TEMP)
    private float minTemperature;
    
    /**
     * maximum temeprature
     */
    @JsonProperty(JSON_MAX_TEMP)
    private float maxTemperature;
    
    public MainInfo()
    {
	// TODO Auto-generated constructor stub
    }
    
    /**
     * constructor
     * @param aCurrentTemperature current temperature
     * @param aMinTemperature minimum temperature for the day.
     * @param aMaxTemperature maximum temperature for the day.
     */
    public MainInfo(float aCurrentTemperature, float aMinTemperature, float aMaxTemperature){
	this.currentTemperature = aCurrentTemperature;
	this.minTemperature = aMinTemperature;
	this.maxTemperature = aMaxTemperature;
    }
    
    /**
     * 
     * @return current temperature
     */
    public float getCurrentTemperature()
    {
	return currentTemperature;
    }
    
    /**
     * 
     * @return maximum temperature
     */
    public float getMaxTemperature()
    {
	return maxTemperature;
    }
    
    /**
     * 
     * @return minimum temperature
     */
    public float getMinTemperature()
    {
	return minTemperature;
    }
    
    @Override
    public String toString()
    {
        return String.format("MainInfo[curr: %f,min: %f,max: %f]", this.getCurrentTemperature(),this.getMinTemperature(),this.getMaxTemperature());
    }
}
