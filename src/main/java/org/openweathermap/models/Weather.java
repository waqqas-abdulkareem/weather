package org.openweathermap.models;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * A Model class that stores generic details about the weather.
 * e.g. Cloudy, Drizzly, Smoke, Mist e.t.c 
 * @author Waqqas
 *
 */
public class Weather implements OWMModel
{
    private static final String JSON_MAIN = "main";
    private static final String JSON_DESCRIPTION = "description";
    
    /**
     * short description of the weather e.g. Cloudy, Misty, Smoke, Clear e.t.c 
     */
    @JsonProperty(JSON_MAIN)
    private String main;
    
    /**
     * A slightly more detailed description of the weather e.g. Scattered clouds.
     */
    @JsonProperty(JSON_DESCRIPTION)
    private String description;
    
    public Weather()
    {
	// TODO Auto-generated constructor stub
    }
    
    /**
     * Constructor 
     * @param main a short description of the weather e.g. Cloudy, Misty, Smoke, Clear. Rain
     * @param aDescription a slightly more detailed description of the weather.
     */
    public Weather(String main,String aDescription)
    {
	this.main = main;
	this.description = aDescription;
    }
    
    /**
     * 
     * @return slightly-longer {@link org.openweathermap.models.Weather#description description} of the weather.
     */
    public String getDescription()
    {
	return description;
    }
    
    /**
     * 
     * @return {@link org.openweathermap.models.Weather#main main} description of the weather.
     */
    public String getMain()
    {
	return main;
    }
    
    @Override
    public String toString()
    {
        return String.format("Weather[main: %s,desc: %s]",  this.getMain(),this.getDescription());
    }
}
