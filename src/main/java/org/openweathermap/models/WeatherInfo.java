package org.openweathermap.models;

import java.util.Date;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonDeserialize;

import com.wks.utils.encoding.JsonUtils;

/**
 * A Model class that stores details about the weather for a city at a certain time.
 * @author Waqqas
 *
 */
public class WeatherInfo implements OWMResponseObject
{
    private static final String JSON_TIMESTAMP = "dt";
    private static final String JSON_CITY_NAME = "name";
    private static final String JSON_MAIN_INFO = "main";
    private static final String JSON_WEATHER = "weather";
    private static final String JSON_SYNOPSIS = "sys";
    private static final String JSON_COORD = "coord";
    
    /**
     * timestamp for when this weather information was received.
     */
    @JsonProperty(JSON_TIMESTAMP)
    @JsonDeserialize(using = JsonUtils.TimeInSecondsDateDeserializer.class)
    private Date timestamp;
    
    /**
     * name of city whose weather information is being recorded.
     */
    @JsonProperty(JSON_CITY_NAME)
    private String cityName;
    
    /**
     * {@link org.openweathermap.models.MainInfo MainInfo} of the city.  
     */
    @JsonProperty(JSON_MAIN_INFO)
    private MainInfo mainInfo;
    
    /**
     * list of {@link org.openweathermap.models.Weather Weather} details for the city.
     */
    @JsonProperty(JSON_WEATHER)
    private List<Weather> weather;
    
    /**
     * {@link org.openweathermap.models.Synopsis Synopsis} of the city.
     */
    @JsonProperty(JSON_SYNOPSIS)
    private Synposis synopsis;
    
    /**
     * {@link org.openweathermap.models.Coordinate coordinate} coordinate of the city.
     */
    @JsonProperty(JSON_COORD)
    private Coordinate coordinates;

    public WeatherInfo()
    {
	// TODO Auto-generated constructor stub
    }

    /**
     * constructor
     * @param aTimestamp {@link java.util.Date timestamp} of when the weather information was gathered.
     * @param aCityName name of city for which weather information is being stored.
     * @param aWeatherCollection collection of {@link org.openweathermap.models.Weather Weather} details.
     * @param aMainInfo {@link org.openweathermap.MainInfo MainInfo} about the weather.
     */
    public WeatherInfo(Date aTimestamp, String aCityName, MainInfo aMainInfo, List<Weather> aWeather,
	    Synposis aSynopsis, Coordinate aCoordinate)
    {
	this.timestamp = aTimestamp;
	this.cityName = aCityName;
	this.mainInfo = aMainInfo;
	this.weather = aWeather;
	this.synopsis = aSynopsis;
	this.coordinates = aCoordinate;
    }

    /**
     * 
     * @return timestamp of when weather information was recorded.
     */
    public Date getTimestamp()
    {
	return timestamp;
    }

    /**
     * 
     * @return name of city
     */
    public String getCityName()
    {
	return cityName;
    }

    /**
     * 
     * @return {@link org.openweathermap.models.MainInfo MainInfo} of city.
     */
    public MainInfo getMainInfo()
    {
	return mainInfo;
    }

    /**
     * 
     * @return collection of {@link org.openweathermap.models.Weather weather} details
     */
    public List<Weather> getWeather()
    {
	return weather;
    }

    /**
     * 
     * @return {@link org.openweathermap.models.Synopsis Synopsis} of city.
     */
    public Synposis getSynopsis()
    {
	return synopsis;
    }

    /**
     * 
     * @return coordinates of the city.
     */
    public Coordinate getCoordinates()
    {
	return coordinates;
    }

    @Override
    public String toString()
    {
	return String.format("Weather[ts: %s, city: %s, main: %s, weather: %s, sys: %s, coord: %s]",
		this.getTimestamp(), this.getCityName(), this.getMainInfo(), this.getWeather(), this.getSynopsis(),
		this.getCoordinates());
    }

    
}
