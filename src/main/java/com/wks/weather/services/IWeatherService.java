package com.wks.weather.services;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.wks.weather.models.ResponseObject;

/**
 * Web Service Public Interface
 * @author Waqqas
 *
 */
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface IWeatherService
{
    /**
     * fetches the current weather of given city
     * @param aCityName name of city
     * @return an implementation of {@link com.wks.weather.models.ResponseObject ResponseObject}
     */
    @GET
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/current")
    public abstract ResponseObject getCurrentWeather(@QueryParam("city") String aCityName);
    
}
