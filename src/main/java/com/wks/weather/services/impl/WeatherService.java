package com.wks.weather.services.impl;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.wks.weather.dao.IWeatherDao;
import com.wks.weather.managers.WeatherInfoManager;
import com.wks.weather.models.ResponseObject;
import com.wks.weather.models.WeatherInfo;
import com.wks.weather.services.IExternalWeatherService;
import com.wks.weather.services.IWeatherService;
import com.wks.weather.services.IExternalWeatherService.Units;
import com.wks.weather.models.Error;

/**
 * Implementation of {@link com.wks.weather.services.impl.IWeatherService
 * IWeatherService}
 * 
 * @author Waqqas
 * 
 */
public class WeatherService implements IWeatherService
{
    /**
     * External Web Service for fetching Weather Information.
     */
    private IExternalWeatherService externalService;

    /**
     * Data Access Object for persting Weather Information.
     */
    private IWeatherDao weatherDao;

    public IWeatherDao getWeatherDao()
    {
	return weatherDao;
    }

    public void setWeatherDao(IWeatherDao weatherDao)
    {
	this.weatherDao = weatherDao;
    }

    public IExternalWeatherService getExternalService()
    {
	return externalService;
    }

    public void setExternalService(IExternalWeatherService externalService)
    {
	this.externalService = externalService;
    }

    @GET
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    @Path("/current")
    public ResponseObject getCurrentWeather(@QueryParam("city") String aCity)
    {
	WeatherInfoManager manager = new WeatherInfoManager(this.getWeatherDao(), this.getExternalService());
	ResponseObject response = null;

	response = manager.getCurrentWeatherInfoFromStorage(aCity);
	System.out.println("DB: " + response);
	if (response == null)
	{

	    response = manager.getWeatherInfoFromExternalService(aCity, Units.METRIC);
	    System.out.println("WEB: " + response);
	    if (response instanceof WeatherInfo)
	    {
		try
		{
		    manager.saveWeatherInfo((WeatherInfo) response);

		} catch (Exception e)
		{
		    e.printStackTrace();
		}
	    } else if (response == null) { return (ResponseObject) new Error(String.format(
		    "Weather could not be retrieved for '%s'.", aCity)); }
	}

	System.out.println("RESP: " + response);
	return response;

    }

}
