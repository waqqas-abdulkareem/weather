package com.wks.weather.services;

import com.wks.weather.models.ExternalServiceResponseObject;

/**
 * Interface that must be implemented by any class that acts as an external (third-party) weather-providing web service.
 * @author Waqqas
 *
 */
public interface IExternalWeatherService
{
    public enum Units{
	METRIC("metric"),
	IMPERIAL("imperial");
	
	private final String type;
	
	private Units(String aType){
	    this.type = aType;
	}
	
	public String getType()
	{
	    return type;
	}
    }
    
    /**
     * current weather information for given city, expressed in given units
     * @param aCityName name of city
     * @param units units of weather measurement e.g imperial, metric
     * @return an implementation of {@link com.wks.weather.models.ExternalServiceResponseObject ExternalServiceResponseObject}
     * @throws Exception if an error occured while fetching weather information.
     */
    public abstract ExternalServiceResponseObject getCurrentWeather(String aCityName, String units) throws Exception; 
}
