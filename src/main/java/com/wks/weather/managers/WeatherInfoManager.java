package com.wks.weather.managers;

import org.springframework.util.Assert;

import com.wks.weather.dao.IWeatherDao;
import com.wks.weather.models.ExternalServiceResponseObject;
import com.wks.weather.models.ResponseObject;
import com.wks.weather.models.WeatherInfo;
import com.wks.weather.models.factory.ResponseObjectFactory;
import com.wks.weather.services.IExternalWeatherService;
import com.wks.weather.models.Error;

/**
 * Class responsible for managing: 
 * <ul>
 * <li>retrieval of {@link com.wks.weather.models.WeatherInfo WeatherInfo} from db or external web service</li>
 * <li>Persisting new {@link com.wks.weather.models.WeatherInfo WeatherInfo} fetched from external web service.</li>
 * </ul>
 * @author Waqqas
 *
 */
public class WeatherInfoManager
{
    
    private final IWeatherDao weatherDao;
    private final IExternalWeatherService extWeatherService;

    /**
     * constructor
     * @param aWeatherDao an implementation of {@link com.wks.weather.dao.IWeatherDao IWeatherDao} for handling persistence of {@link com.wks.weather.models.WeatherInfo WeatherInfo}
     * @param anExtWeatherService an implementation of {@link com.wks.weather.services.IExternalWeatherService IExternalWeatherService} for fetching WeatherInfo from external web service.
     * @throws IllegalArgumentException if aWeatherDao is null.
     * @throws IllegalArgumentException if anExtWeatherService is null.
     */
    public WeatherInfoManager(IWeatherDao aWeatherDao, IExternalWeatherService anExtWeatherService)
    {
	Assert.notNull(aWeatherDao);
	Assert.notNull(anExtWeatherService);
	
	this.weatherDao = aWeatherDao;
	this.extWeatherService = anExtWeatherService;
    }

    /**
     * retrieves {@link com.wks.weather.models.WeatherInfo weatherInfo} for given city on today's date from database.
     * @param aCityName name of city
     * @return an instance of {@link com.wks.weather.models.WeatherInfo WeatherInfo} if record found in database<br/>
     * <code>null</code> if there is no weahterInfo for given city on today's date.
     * @throws IllegalArgumentException if aCityName is null or empty.
     */
    public WeatherInfo getCurrentWeatherInfoFromStorage(String aCityName){	
	return this.weatherDao.getCurrentWeather(aCityName);
    }

    /**
     * retrieves {@link com.wks.weather.models.WeatherInfo weatherInfo} for given city on today's date from external weather service.
     * @param aCityName name of city
     * @param units {@link com.wks.weather.services.IExternalWeatherService.Units Units} of weather info measurement
     * @return instance of  {@link com.wks.weather.models.WeatherInfo weatherInfo} if request was successful.<br/>
     * instance of {@link com.wks.weather.models.Error error} if an error occurred.
     * @throws IllegalArgumentException if aCityName is null or empty.
     * @throws IllegalArgumentException if units is null.
     */
    public ResponseObject getWeatherInfoFromExternalService(String aCityName,IExternalWeatherService.Units units){	
	try
	{
	    ExternalServiceResponseObject extResp = this.extWeatherService.getCurrentWeather(aCityName, units.getType());
	    return ResponseObjectFactory.fromExternalServiceResponeObject(extResp);
	} catch (Exception e)
	{
	    e.printStackTrace();
	    return new Error("Weather Info not available.");
	}
	
    }
    
    /**
     * saves given instance of {@link com.wks.weather.models.WeatherInfo weatherInfo} to database.
     * @param aWeatherInfo instance of {@link com.wks.weather.models.WeatherInfo weatherInfo}
     * @return <code>true</code> if instance persisted successfully, otherwise <code>false</code>.
     * @throws Exception if aWeatherInfo is null.
     */
    public boolean saveWeatherInfo(WeatherInfo aWeatherInfo) throws Exception{
	
	return this.weatherDao.saveWeather(aWeatherInfo);
    }
}
