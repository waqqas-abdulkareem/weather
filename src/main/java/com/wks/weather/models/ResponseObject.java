package com.wks.weather.models;

/**
 * an interface that must be implemented by any object that is sent as a response by this web service.
 * @author Waqqas
 *
 */
public interface ResponseObject
{

}
