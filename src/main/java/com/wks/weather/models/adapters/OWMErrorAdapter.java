package com.wks.weather.models.adapters;

import com.wks.weather.models.Error;
import com.wks.weather.models.ExternalServiceResponseObject;
import com.wks.weather.models.ResponseObject;

/**
 * An adapter class that adapts OpenWeatherMap's
 * {@link org.openweathermap.models.Error Error} object to this web service's
 * {@link com.wks.weather.models.Error Error} object.
 * 
 * @author Waqqas
 * 
 */
public class OWMErrorAdapter implements IResponseAdapter
{

    /**
     * Adapts OpenWeatherMap's {@link org.openweathermap.models.Error Error} object to this webservice's {@link com.wks.weather.models.Error Error} object.
     * @param extResp an instance of OpenWeatherMap {@link org.openweathermap.models.Error Error}
     * @return instance of this web servie's {@link com.wks.weather.models.Error Error}.
     * @throws ClassCastException if extResp is not an instance of OpenWeatherMap {@link org.openweathermap.models.Error Error}
     */
    public ResponseObject convert(ExternalServiceResponseObject extResp)
    {
	org.openweathermap.models.Error owmError = (org.openweathermap.models.Error) extResp;
	return new Error(owmError.getMessgae());
    }
}
