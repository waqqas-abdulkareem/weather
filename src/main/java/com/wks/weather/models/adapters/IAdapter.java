package com.wks.weather.models.adapters;

/**
 * Generic interface to be implemented by a class that adapts one kind of model into another kind of model
 * @author Waqqas
 *
 * @param <TFrom> source model which will be adapted.
 * @param <TTo> destination model.
 */
public interface IAdapter<TFrom, TTo>
{
    /**
     * adapts fromObject to a destination model.
     * @param fromObject model object which is to be adapted to another model.
     * @return instance of destination model.
     */
    public abstract TTo convert(TFrom fromObject);
}
