package com.wks.weather.models.adapters;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.openweathermap.models.Synposis;

import com.wks.weather.models.City;
import com.wks.weather.models.Coordinate;
import com.wks.weather.models.ExternalServiceResponseObject;
import com.wks.weather.models.MainInfo;
import com.wks.weather.models.Weather;
import com.wks.weather.models.WeatherInfo;

/**
 * An adapter class that adapts OpenWeatherMap's
 * {@link org.openweathermap.models.WeatherInfo WeatherInfo} object to this web service's
 * {@link com.wks.weather.models.WeatherInfo WeatherInfo} object.
 * 
 * @author Waqqas
 * 
 */
public class OWMWeatherInfoAdapter implements IResponseAdapter
{
    /**
     * maps data from OpenWeatherMap {@link org.openweathermap.models.WeatherInfo WeatherInfo} to {@link com.wks.weather.models.City City}.
     * @param aWeatherInfo an instance of OpenWeatherMap {@link org.openweathermap.models.WeatherInfo WeatherInfo}
     * @return an instance of {@link com.wks.weather.models.City City} with details mapped from OpenWeatherMap {@link org.openweathermap.models.WeatherInfo WeatherInfo}.
     */
    private City getCity(org.openweathermap.models.WeatherInfo aWeatherInfo){
	assert aWeatherInfo != null;
	
	Synposis sys = aWeatherInfo.getSynopsis();
	Coordinate coord = new Coordinate(aWeatherInfo.getCoordinates().getLongitude(),aWeatherInfo.getCoordinates().getLatitude());
	return new City(aWeatherInfo.getCityName(),sys.getCountry(),sys.getSunrise(),sys.getSunset(),coord);
    }

    /**
     * maps data from OpenWeatherMap {@link org.openweathermap.models.WeatherInfo WeatherInfo} to {@link com.wks.weather.models.MainInfo MainInfo}.
     * @param aWeatherInfo an instance of OpenWeatherMap {@link org.openweathermap.models.WeatherInfo WeatherInfo}
     * @return an instance of {@link com.wks.weather.models.City MainInfo} with details mapped from OpenWeatherMap {@link org.openweathermap.models.WeatherInfo WeatherInfo}.
     */
    private com.wks.weather.models.MainInfo getMainInfo(org.openweathermap.models.WeatherInfo aWeatherInfo){
	org.openweathermap.models.MainInfo main = aWeatherInfo.getMainInfo();
	return new com.wks.weather.models.MainInfo(main.getCurrentTemperature(),main.getMinTemperature(),main.getMaxTemperature());
    }
    
    /**
     * maps data from OpenWeatherMap {@link org.openweathermap.models.WeatherInfo WeatherInfo} to {@link com.wks.weather.models.WeatherInfo#weathers weathers}.
     * @param aWeatherInfo an instance of OpenWeatherMap {@link org.openweathermap.models.WeatherInfo WeatherInfo}
     * @return an collection of {@link com.wks.weather.models.WeatherInfo#weathers weathers} with details from OpenWeatherMap {@link org.openweathermap.models.WeatherInfo WeatherInfo}.
     */
    private Collection<Weather> getWeatherCollection(org.openweathermap.models.WeatherInfo aWeatherInfo){
	
	Collection<org.openweathermap.models.Weather> owmWeathers = aWeatherInfo.getWeather();
	Collection<Weather> weathers = new ArrayList<Weather>();
	for(org.openweathermap.models.Weather owmWeather : owmWeathers){
	    weathers.add(new Weather(owmWeather.getMain(),owmWeather.getDescription()));
	}
	
	return weathers;
    }
    
    /**
     * Adapts OpenWeatherMap's {@link org.openweathermap.models.WeatherInfo WeatherInfo} object to this webservice's {@link com.wks.weather.models.WeatherInfo WeatherInfo} object.
     * @param extResp an instance of OpenWeatherMap {@link org.openweathermap.models.WeatherInfo WeatherInfo}
     * @return instance of this web servie's {@link com.wks.weather.models.WeatherInfo WeatherInfo}.
     * @throws ClassCastException if extResp is not an instance of OpenWeatherMap {@link org.openweathermap.models.WeatherInfo WeatherInfo}
     */
    public WeatherInfo convert(ExternalServiceResponseObject extResp)
    {
	org.openweathermap.models.WeatherInfo owmWeatherInfo = (org.openweathermap.models.WeatherInfo) extResp;
	Date date = owmWeatherInfo.getTimestamp();
	City city = getCity(owmWeatherInfo);
	MainInfo mainInfo = getMainInfo(owmWeatherInfo);
	Collection<Weather> weather = getWeatherCollection(owmWeatherInfo);
	
	return new WeatherInfo(date,city,weather,mainInfo);
    }
}
