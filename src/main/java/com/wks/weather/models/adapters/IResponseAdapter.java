package com.wks.weather.models.adapters;

import com.wks.weather.models.ExternalServiceResponseObject;
import com.wks.weather.models.ResponseObject;

/**
 * Interface that is to be implemented by an adapter class that converts an 
 * {@link com.wks.weather.models.ExternalServiceResponseObject
 * ExternalServiceResponseObject} to this web service's
 * {@link com.wks.weather.models.ResponseObject ResponseObject}
 * 
 * @author Waqqas
 * 
 */
public interface IResponseAdapter extends IAdapter<ExternalServiceResponseObject, ResponseObject>
{

}
