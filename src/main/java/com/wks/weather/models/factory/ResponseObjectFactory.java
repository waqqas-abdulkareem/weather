package com.wks.weather.models.factory;

import org.openweathermap.models.OWMResponseObject;

import com.wks.weather.models.ExternalServiceResponseObject;
import com.wks.weather.models.ResponseObject;

/**
 * A Factory class that produces a {@link com.wks.weather.models.ResponseObject ResponseObject} from an {@link com.wks.weather.models.ExternalServiceResponseObject ExternalServiceResponseObject}
 * @author Waqqas
 *
 */
public class ResponseObjectFactory
{
    /**
     * produces a {@link com.wks.weather.models.ResponseObject ResponseObject} from an {@link com.wks.weather.models.ExternalServiceResponseObject ExternalServiceResponseObject}.
     * @param extResp an {@link com.wks.weather.models.ExternalServiceResponseObject ExternalServiceResponseObject}
     * @return a {@link com.wks.weather.models.ResponseObject ResponseObject}
     */
    public static ResponseObject fromExternalServiceResponeObject(ExternalServiceResponseObject extResp){
	if(extResp == null){
	   return null;
	}
	
	if(extResp instanceof OWMResponseObject){
	    return OWMResponseAdapterFactory.fromOWMResponseObject((OWMResponseObject)extResp).convert(extResp);
	}else{
	    throw new IllegalArgumentException(String.format("instance of %s can not be converted to %s.", extResp.getClass(),ResponseObject.class));
	}
    }
}
