package com.wks.weather.models.factory;

import org.openweathermap.models.OWMResponseObject;
import org.springframework.util.Assert;

import com.wks.weather.models.adapters.IResponseAdapter;
import com.wks.weather.models.adapters.OWMErrorAdapter;
import com.wks.weather.models.adapters.OWMWeatherInfoAdapter;

/**
 * A Factory class that produces an adapter that converts this web service's {@link com.wks.weather.models.ResponseObject ResponseObjects} from OpenWeatherMap's {@link org.openweathermap.models.OMWResponseObject ResponseObjects}
 * @author Waqqas
 *
 */
public class OWMResponseAdapterFactory
{
    /**
     * 
     * @param owmResp an instance of {@link org.openweathermap.models.OWMResponseObject OWMResponseObject}
     * @return a {@link com.wks.weather.models.adapters.IResponseAdapter ResponseAdapter} that can convert an {@link org.openweathermap.models.OWMResponseObject OWMResponseObject} to a {@link com.wks.weather.models.ResponseObject ResponseObject} 
     * 
     */
    public static IResponseAdapter fromOWMResponseObject(OWMResponseObject owmResp){
	Assert.notNull(owmResp,"Can not return adapter for null object");
	
	if(owmResp instanceof org.openweathermap.models.WeatherInfo){
	    return new OWMWeatherInfoAdapter();
	}else if(owmResp instanceof org.openweathermap.models.Error){
	    return new OWMErrorAdapter();
	}else{
	    throw new IllegalArgumentException(String.format("%s can not handle owm response object of type %s",OWMResponseAdapterFactory.class,owmResp.getClass()));
	}
    }
}
