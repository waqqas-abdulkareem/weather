package com.wks.weather.models;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.wks.utils.encoding.JsonUtils;

/**
 * A Model class that stores details about the weather for a city at a certain time.
 * @author Waqqas
 *
 */
@Entity
@Cacheable
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
public class WeatherInfo implements ResponseObject
{
    /**
     * name of memeber field used to store {@link com.wks.weather.models.WeatherInfo#date date}.<br/>
     * Referenced for JPA Queries.
     */
    public static final String FIELD_DATE = "date";
    /**
     * name of member field used to store {@link com.wks.weather.models.WeatherInfo#city city}.<br/>
     * Referenced for JPA Queries.
     */
    public static final String FIELD_CITY = "city";
    
    private static final String JSON_DATE = "date";
    private static final String JSON_CITY = "city";
    private static final String JSON_WEATHER = "weather";
    private static final String JSON_MAIN_INFO = "main";
    
    /**
     * id of the weather info, generated automatically by the database.
     */
    @Id
    @GeneratedValue
    private int id;
    
    /**
     * timestamp for when this weather information was received.
     */
    @Column
    @Temporal(value=TemporalType.TIMESTAMP)
    @JsonProperty(JSON_DATE)
    @JsonSerialize(using=JsonUtils.TimestampSerializer.class)
    private Date date;
    
    /**
     * city whose weather information is being stored.
     */
    @OneToOne
    @JsonProperty(JSON_CITY)
    private City city;
    
    /**
     * list of {@link com.wks.weather.models.Weather Weather} details for the city.
     */
    @ManyToMany(cascade=CascadeType.ALL,fetch=FetchType.EAGER)
    @JsonProperty(JSON_WEATHER)
    private Collection<Weather> weather;
    
    /**
     * {@link com.wks.weather.models.MainInfo MainInfo} of the city.  
     */
    @OneToOne
    @JsonProperty(JSON_MAIN_INFO)
    private MainInfo mainInfo;
    
    @SuppressWarnings("unused")
    private WeatherInfo()
    {
	//Used by Hibernate
    }
    
    /**
     * constructor
     * @param aDate {@link java.util.Date timestamp} of when the weather information was gathered.
     * @param aCity {@link com.wks.weather.models.City city} for which weather information is being stored.
     * @param aWeatherCollection collection of {@link com.wks.weather.models.Weather Weather} details.
     * @param aMainInfo {@link com.wks.weather.models.MainInfo MainInfo} about the weather.
     */
    public WeatherInfo(Date aDate, City aCity,Collection<Weather> aWeatherCollection, MainInfo aMainInfo){
	this.date = aDate;
	this.city = aCity;
	this.weather = aWeatherCollection;
	this.mainInfo = aMainInfo;
    }
    
    /**
     * 
     * @return {@link java.util.Date timestamp} when weather data was gathered.
     */
    public Date getDate()
    {
	return date;
    }
    
    /**
     * sets the {@link com.wks.weather.models.City city} whose weather information is being stored.
     * @param city details of the {@link com.wks.weather.models.City city} 
     */
    public void setCity(City city)
    {
	this.city = city;
    }
    
    /**
     * 
     * @return {@link com.wks.weather.models.City city} whose weather information is being stored.
     */
    public City getCity()
    {
	return city;
    }
    
    /**
     * 
     * @return collection of {@link com.wks.weather.models.Weather weather} details
     */
    public Collection<Weather> getWeathers()
    {
	return weather;
    }
    
    /**
     * sets the {@link com.wks.weather.models.Weather weather} details
     * @param weather
     */
    public void setWeathers(Collection<Weather> weather)
    {
	this.weather = weather;
    }
    
    /**
     * 
     * @return {@link com.wks.weather.models.MainInfo MainInfo} about the weather.
     */
    public MainInfo getMainInfo()
    {
	return mainInfo;
    }
    
    /**
     * sets {@link com.wks.weather.models.MainInfo MainInfo} about the weather.
     * @param mainInfo {@link com.wks.weather.models.MainInfo MainInfo} about the weather
     */
    public void setMainInfo(MainInfo mainInfo)
    {
	this.mainInfo = mainInfo;
    }
    
    @Override
    public String toString()
    {
       return String.format("WeatherInfo[date: %s, city: %s, weather: %s, mainInfo: %s]",this.getDate(),this.getCity(),this.getWeathers(),this.getMainInfo());
    }
}
