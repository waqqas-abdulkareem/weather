package com.wks.weather.models;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * A model class that stores details of an error encountered by the web service.
 * @author Waqqas
 *
 */
public class Error implements ResponseObject
{
    private static final String JSON_MESSAGE = "message";
    
    @JsonProperty(JSON_MESSAGE)
    private final String message;
    
    /**
     * constructor
     * @param aMessage description of the error.
     */
    public Error(String aMessage)
    {
	this.message = aMessage;
    }
    
    /**
     * 
     * @return description of the error.
     */
    public String getMessage()
    {
	return message;
    }
    
    @Override
    public String toString()
    {
        return String.format("Error[message: %s]", this.getMessage());
    }
}
