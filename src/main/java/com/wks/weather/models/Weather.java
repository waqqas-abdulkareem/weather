package com.wks.weather.models;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.codehaus.jackson.annotate.JsonProperty;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Model class that stores generic details about the weather.
 * e.g. Cloudy, Drizzly, Smoke, Mist e.t.c 
 * @author Waqqas
 *
 */
@Entity
@Cacheable
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
public class Weather
{
    public static final String FIELD_MAIN = "main";

    private static final String JSON_MAIN = "main";
    private static final String JSON_DESCRIPTION = "description";

    /**
     * id of the Weather, generated automatically by the database.
     */
    @Id
    @GeneratedValue
    private int id;

    /**
     * short description of the weather e.g. Cloudy, Misty, Smoke, Clear e.t.c 
     */
    @Column
    @JsonProperty(JSON_MAIN)
    private String main;

    /**
     * A slightly more detailed description of the weather e.g. Scattered clouds.
     */
    @Column
    @JsonProperty(JSON_DESCRIPTION)
    private String description;

    @SuppressWarnings("unused")
    private Weather()
    {
	// Used by Hibernate
    }

    /**
     * Constructor 
     * @param main a short description of the weather e.g. Cloudy, Misty, Smoke, Clear. Rain
     * @param aDescription a slightly more detailed description of the weather.
     */
    public Weather(String main, String aDescription)
    {
	this.main = main;
	this.description = aDescription;
    }

    /**
     * 
     * @return {@link com.wks.weather.models.Weather#main main} description of the weather.
     */
    public String getMain()
    {
	return main;
    }

    /**
     * 
     * @return slightly-longer {@link com.wks.weather.models.Weather#description description} of the weather.
     */
    public String getDescription()
    {
	return description;
    }

    @Override
    public String toString()
    {
	return String.format("Weather[main: %s,desc: %s]", this.getMain(), this.getDescription());
    }
}
