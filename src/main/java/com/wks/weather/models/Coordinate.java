package com.wks.weather.models;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.codehaus.jackson.annotate.JsonProperty;

@Embeddable
/**
 * A model class used to store the coordinates of a city.
 * @author Waqqas
 *
 */
public class Coordinate
{
    private static final String JSON_LONGITUDE = "lon";
    private static final String JSON_LATITUDE = "lat";
    
    @Column
    @JsonProperty(JSON_LONGITUDE)
    /**
     * longitude of the city.
     */
    private float longitude;
    
    @Column
    @JsonProperty(JSON_LATITUDE)
    /**
     * latitude of city.
     */
    private float latitude;
    
    @SuppressWarnings("unused")
    private Coordinate(){
	//Used by Hibernate
    }
    
    /**
     * constructor
     * @param aLongitude longitude of city.
     * @param aLatitude latitude of city.
     */
    public Coordinate(float aLongitude,float aLatitude){
	this.longitude = aLongitude;
	this.latitude = aLatitude;
    }
    
    /**
     * sets the longitude of the city.
     * @param longitude 
     */
    public void setLongitude(float longitude)
    {
	this.longitude = longitude;
    }
    
    /**
     * 
     * @return longitude of city.
     */
    public float getLongitude()
    {
	return longitude;
    }
    
    /**
     * sets the latitude of the city.
     * @param latitude
     */
    public void setLatitude(float latitude)
    {
	this.latitude = latitude;
    }
    
    /**
     * 
     * @return latitude of city.
     */
    public float getLatitude()
    {
	return latitude;
    }
    
    @Override
    public String toString()
    {
        return String.format("Coordinate[lat: %f,lon: %s]", this.getLatitude(),this.getLongitude());
    }
}
