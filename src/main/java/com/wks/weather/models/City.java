package com.wks.weather.models;

import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.wks.utils.encoding.JsonUtils;

/**
 * A model class that is used to store details about the city whose weather is
 * being recorded.
 * 
 * @author Waqqas
 * 
 */
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class City
{
    /**
     * name of member variable used to story city name. Referenced by Hibernate
     * queries.
     */
    public static final String FIELD_NAME = "name";

    private static final String JSON_NAME = "name";
    private static final String JSON_COUNTRY = "country";
    private static final String JSON_SUNRISE = "sunrise";
    private static final String JSON_SUNSET = "sunset";
    private static final String JSON_COORD = "coord";

    @Id
    @GeneratedValue
    /**
     * id of city assigned automatically by the database.
     */
    private int id;

    @Column
    @JsonProperty(JSON_NAME)
    /**
     * name of the city.
     */
    private String name;

    @Column
    @JsonProperty(JSON_COUNTRY)
    /**
     * name of the country in which the city is located.
     */
    private String country;

    @Column
    @Temporal(value = TemporalType.TIMESTAMP)
    @JsonProperty(JSON_SUNRISE)
    @JsonSerialize(using = JsonUtils.TimeSerializer.class)
    /**
     * time of sunrise in the city.
     */
    private Date sunrise;

    @Column
    @Temporal(value = TemporalType.TIMESTAMP)
    @JsonProperty(JSON_SUNSET)
    @JsonSerialize(using = JsonUtils.TimeSerializer.class)
    /**
     * time of sunset in the city.
     */
    private Date sunset;

    @Embedded
    @JsonProperty(JSON_COORD)
    /**
     * coordinates of the city.
     */
    private Coordinate coordinate;

    @SuppressWarnings("unused")
    private City()
    {
	// Used by Hibernate
    }

    /**
     * constructor
     * 
     * @param aName
     *            name of city
     * @param aCountry
     *            name of country
     * @param aSunriseTime
     *            sunrise time.
     * @param aSunsetTime
     *            sunset time.
     * @param aCoordiante
     *            coordinates of the city.
     */
    public City(String aName, String aCountry, Date aSunriseTime, Date aSunsetTime, Coordinate aCoordinate)
    {
	this.setName(aName);
	this.setCountry(aCountry);
	this.setSunrise(aSunriseTime);
	this.setSunset(aSunsetTime);
	this.setCoordinate(aCoordinate);
    }

    /**
     * 
     * @return name of city.
     */
    public String getName()
    {
	return name;
    }

    /**
     * sets name of city.
     * 
     * @param aName
     *            name of city.
     */
    public void setName(String aName)
    {
	this.name = aName;
    }

    /**
     * 
     * @return name of country.
     */
    public String getCountry()
    {
	return country;
    }

    /**
     * sets name of country
     * 
     * @param aCountry
     *            name of country
     */
    public void setCountry(String aCountry)
    {
	this.country = aCountry;
    }

    /**
     * 
     * @return time of sunrise.
     */
    public Date getSunrise()
    {
	return sunrise;
    }

    /**
     * sets sunrise time.
     * 
     * @param aSunriseTime
     *            sunrise time.
     */
    public void setSunrise(Date aSunriseTime)
    {
	this.sunrise = aSunriseTime;
    }

    /**
     * 
     * @return time of sunset in city.
     */
    public Date getSunset()
    {
	return sunset;
    }

    /**
     * sets time of sunset in city.
     * 
     * @param aSunsetTime
     *            time of sunset.
     * @throws IllegalArgumentException
     *             if aSunsetTime is null.
     */
    public void setSunset(Date aSunsetTime)
    {
	this.sunset = aSunsetTime;
    }

    /**
     * 
     * @return coordinates of city.
     */
    public Coordinate getCoordinate()
    {
	return coordinate;
    }

    /**
     * sets coordinates of city.
     * 
     * @param coordinate
     */
    public void setCoordinate(Coordinate coordinate)
    {
	this.coordinate = coordinate;
    }

    @Override
    public String toString()
    {
	return String.format("City[name: %s, country: %s, sunrise: %s, sunset: %s, coord: %s]", this.getName(),
		this.getCountry(), this.getSunrise(), this.getSunset(), this.getCoordinate());
    }
}
