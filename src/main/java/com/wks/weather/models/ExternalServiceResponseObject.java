package com.wks.weather.models;

/**
 * Interface that must be implemented by the response objects returned by {@link com.wks.weather.services.IExternalWeatherService External Weather Services}.
 * @author Waqqas
 *
 */
public interface ExternalServiceResponseObject
{

}
