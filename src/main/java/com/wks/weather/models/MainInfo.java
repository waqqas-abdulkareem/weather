package com.wks.weather.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Model class  that stores main details about the weather e.g.<br/>
 * Temperature (Current, Minimum, Maximum)<br/>
 * 	 
 * @author Waqqas
 *
 */
@Entity
public class MainInfo
{
    private static final String JSON_CURRENT_TEMP = "temp";
    private static final String JSON_MIN_TEMP = "temp_min";
    private static final String JSON_MAX_TEMP = "temp_max";
    
    /**
     * id of the MainInfo record, geenrated automatically by the database.
     */
    @Id
    @GeneratedValue
    private int id;
    
    /**
     * current temperature
     */
    @Column
    @JsonProperty(JSON_CURRENT_TEMP)
    private float currentTemperature;
    
    /**
     * minimum temperature.
     */
    @Column
    @JsonProperty(JSON_MIN_TEMP)
    private float minTemperature;
    
    /**
     * maximum temperature
     */
    @Column
    @JsonProperty(JSON_MAX_TEMP)
    private float maxTemperature;
    
    @SuppressWarnings("unused")
    private MainInfo(){
	//Used by Hibernate
    }
    
    /**
     * constructor
     * @param aCurrentTemperature current temperature
     * @param aMinTemperature minimum temperature for the day.
     * @param aMaxTemperature maximum temperature for the day.
     */
    public MainInfo(float aCurrentTemperature, float aMinTemperature, float aMaxTemperature){
	this.currentTemperature = aCurrentTemperature;
	this.minTemperature = aMinTemperature;
	this.maxTemperature = aMaxTemperature;
    }
    
    /**
     * 
     * @return current temperature
     */
    public float getCurrentTemperature()
    {
	return currentTemperature;
    }
    
    /**
     * sets the current temperature
     * @param currentTemperature current temperature
     */
    public void setCurrentTemperature(float currentTemperature)
    {
	this.currentTemperature = currentTemperature;
    }
    
    /**
     * 
     * @return minimum temperature of the day.
     */
    public float getMinTemperature()
    {
	return minTemperature;
    }
    
    /**
     * sets minimum temperature of the day
     * @param minTemperature minimum temperature
     */
    public void setMinTemperature(float minTemperature)
    {
	this.minTemperature = minTemperature;
    }
    
    /**
     * 
     * @return max temperature of the day.
     */
    public float getMaxTemperature()
    {
	return maxTemperature;
    }
    
    /**
     * sets maximum temperature of the day
     * @param maxTemperature maximum temperature
     */
    public void setMaxTemperature(float maxTemperature)
    {
	this.maxTemperature = maxTemperature;
    }
    
    @Override
    public String toString()
    {
        return String.format("MainInfo[temp: %f,min: %f, max: %f]", this.getCurrentTemperature(),this.getMinTemperature(),this.getMaxTemperature());
    }
}
