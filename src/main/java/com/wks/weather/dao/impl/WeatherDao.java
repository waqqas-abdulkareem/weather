package com.wks.weather.dao.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import org.apache.commons.lang3.text.WordUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import com.wks.utils.encoding.DateTimeUtils;
import com.wks.utils.persistence.HibernateUtils;
import com.wks.weather.dao.IWeatherDao;
import com.wks.weather.models.City;
import com.wks.weather.models.MainInfo;
import com.wks.weather.models.Weather;
import com.wks.weather.models.WeatherInfo;

/**
 * Implementation of {@link com.wks.weather.dao.IWeatherDao IWeatherDao}
 * 
 * @author Waqqas
 * 
 */
public class WeatherDao implements IWeatherDao
{

    private static final boolean CACHE_GET_CITY_CRITERIA = true;
    private static final boolean CACHE_GET_WEATHER_CRITERIA = true;
    private static final boolean CACHE_GET_WEATHER_INFO_CRITERIA = false;
    private static final boolean CACHE_GET_CURRENT_WEATHER_CRITERIA = false;

    /**
     * 
     * @param aCityName
     *            name of city
     * @return instance of {@link com.wks.weather.models.City City} whose name
     *         matches given name (case-sensitive).<br/>
     *         <code>null</code> if no matching city is found.<br/>
     *         <code>null</code> if aCityName is <code>null</code> or empty.
     */
    public City getCity(String aCityName)
    {
	if (aCityName == null || aCityName.trim().isEmpty()) { return null; }
	aCityName = WordUtils.capitalizeFully(aCityName);

	Session session = HibernateUtils.openSession();
	City city = null;

	try
	{
	    session.beginTransaction();

	    Criteria criteria = session.createCriteria(City.class);
	    criteria.add(Restrictions.ilike(City.FIELD_NAME, aCityName, MatchMode.EXACT)).setCacheable(
		    CACHE_GET_CITY_CRITERIA);

	    city = (City) criteria.uniqueResult();

	    session.getTransaction().commit();
	} catch (HibernateException e)
	{
	    e.printStackTrace();
	    session.getTransaction().rollback();
	} finally
	{
	    session.close();
	}

	return city;
    }

    public Weather getWeather(String main)
    {
	if (main == null || main.trim().isEmpty()) { return null; }

	Session session = HibernateUtils.openSession();
	Weather weather = null;

	try
	{
	    session.beginTransaction();

	    Criteria criteria = session.createCriteria(Weather.class);
	    criteria.add(Restrictions.eq(Weather.FIELD_MAIN, main));
	    criteria.setCacheable(CACHE_GET_WEATHER_CRITERIA);

	    weather = (Weather) criteria.uniqueResult();

	    session.getTransaction().commit();
	} catch (HibernateException e)
	{
	    e.printStackTrace();
	    session.getTransaction().rollback();
	} finally
	{
	    session.close();
	}

	return weather;
    }

    public WeatherInfo getWeatherInfo(String aCityName)
    {
	if (aCityName == null || aCityName.trim().isEmpty()) { return null; }
	aCityName = WordUtils.capitalizeFully(aCityName);

	Session session = HibernateUtils.openSession();
	WeatherInfo info = null;

	try
	{
	    session.beginTransaction();

	    Criteria criteria = session.createCriteria(WeatherInfo.class);
	    criteria.createAlias(WeatherInfo.FIELD_CITY, "c")
		    .add(Restrictions.ilike("c." + City.FIELD_NAME, aCityName, MatchMode.EXACT))
		    .setCacheable(CACHE_GET_WEATHER_INFO_CRITERIA);

	    info = (WeatherInfo) criteria.uniqueResult();

	    session.getTransaction().commit();
	} catch (HibernateException e)
	{
	    e.printStackTrace();
	    session.getTransaction().rollback();
	} finally
	{
	    session.close();
	}

	return info;
    }

    public WeatherInfo getCurrentWeather(String aCity)
    {
	if (aCity == null || aCity.trim().isEmpty()) { return null; }
	aCity = WordUtils.capitalizeFully(aCity);
	
	Session session = HibernateUtils.openSession();
	WeatherInfo weatherInfo = null;
	Date midnight = DateTimeUtils.getMidnight(new Date());
	try
	{
	    session.beginTransaction();

	    System.out.printf("Between %s and %s\n", midnight, DateTimeUtils.addDays(midnight, 1));

	    Criteria criteria = session.createCriteria(WeatherInfo.class).createAlias(WeatherInfo.FIELD_CITY, "c")
		    .add(Restrictions.ilike("c." + City.FIELD_NAME, aCity, MatchMode.EXACT))
		    .add(Restrictions.ge(WeatherInfo.FIELD_DATE, midnight))
		    .add(Restrictions.lt(WeatherInfo.FIELD_DATE, DateTimeUtils.addDays(midnight, 1)))
		    .setCacheable(CACHE_GET_CURRENT_WEATHER_CRITERIA);

	    weatherInfo = (WeatherInfo) criteria.uniqueResult();

	    session.getTransaction().commit();

	} catch (Exception e)
	{
	    e.printStackTrace();
	    session.getTransaction().rollback();
	} finally
	{
	    session.close();
	}

	return weatherInfo;
    }

    public boolean saveWeather(WeatherInfo aWeatherInfo)
    {

	Session session = HibernateUtils.openSession();
	boolean successful = false;
	try
	{
	    session.beginTransaction();

	    // if WeatherInfo already saved for this city, get record.
	    WeatherInfo persistentInfo = this.getWeatherInfo(aWeatherInfo.getCity().getName());
	    if (persistentInfo == null)
	    {
		persistentInfo = aWeatherInfo;
	    }

	    // save or update city's sunset and sunrise information.
	    City city = persistentInfo.getCity();
	    city.setSunrise(aWeatherInfo.getCity().getSunrise());
	    city.setSunset(aWeatherInfo.getCity().getSunset());
	    session.saveOrUpdate(city);

	    // save or update city's main weather information (namely,
	    // temperature)
	    MainInfo mainInfo = persistentInfo.getMainInfo();
	    mainInfo.setCurrentTemperature(aWeatherInfo.getMainInfo().getCurrentTemperature());
	    mainInfo.setMinTemperature(aWeatherInfo.getMainInfo().getMinTemperature());
	    mainInfo.setMaxTemperature(aWeatherInfo.getMainInfo().getMaxTemperature());
	    session.saveOrUpdate(mainInfo);

	    // save or update city's weather information.
	    persistentInfo.setWeathers(persistWeather(session, aWeatherInfo));

	    // save or update persistent info.
	    session.save(persistentInfo);

	    session.getTransaction().commit();
	    successful = true;
	} catch (Exception e)
	{
	    e.printStackTrace();
	    session.getTransaction().rollback();
	} finally
	{
	    session.close();
	}

	return successful;
    }

    /**
     * Iterates over {@link com.wks.weather.models.WeatherInfo#weathers
     * weathers} and replaces them with persisted
     * {@link com.wks.weather.models.Weather Weather} objects, where necessary.
     * 
     * @param session
     *            open transaction session.
     * @param aWeatherInfo
     *            aWeatherInfo whose {@link
     *            com.wks.weather.models.WeatherInfo#weathers weathers} are to be persisted.
     * @return a collection of {@link com.wks.weather.models.WeatherInfo#weathers
     *         weathers} containing persisted
     *         {@link com.wks.weather.models.Weather Weather} objects
     */
    private Collection<Weather> persistWeather(Session session, WeatherInfo aWeatherInfo)
    {
	assert aWeatherInfo != null;
	assert session != null;
	assert session.isOpen();

	Collection<Weather> persistedWeathers = new ArrayList<Weather>();
	Iterator<Weather> weathers = aWeatherInfo.getWeathers().iterator();
	while (weathers.hasNext())
	{
	    Weather weather = weathers.next();
	    Weather persistedWeather = this.getWeather(weather.getMain());
	    if (persistedWeather == null)
	    {
		session.save(weather);
		persistedWeather = weather;
	    }

	    persistedWeathers.add(persistedWeather);
	}

	return persistedWeathers;
    }
}
