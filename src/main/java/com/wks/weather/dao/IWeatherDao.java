package com.wks.weather.dao;

import com.wks.weather.models.City;
import com.wks.weather.models.Weather;
import com.wks.weather.models.WeatherInfo;

/**
 * Persistence interface for {@link com.wks.weather.models.WeatherInfo WeatherInfo}
 * @author Waqqas
 *
 */
public interface IWeatherDao
{
    /**
     * retrieves {@link com.wks.weather.models.City City} whose name matches given city name from the database.
     * @param aCityName name of city 
     * @return instance of {@link com.wks.weather.models.City City} or <code>null</code> if no matching city found.
     */
    public abstract City getCity(String aCityName);
    
    /**
     * retreives {@link com.wks.weather.models.Weather Weather} whose {@link com.wks.weather.models.Weather#main main} matches the one provided from the database.
     * @param aMain type of Weather (Clear, Clouds, Rain, Drizzle.)
     * @return instance of {@link com.wks.weather.models.Weather Weather} or <code>null</code> if matching main not found.
     */
    public abstract Weather getWeather(String aMain);
    
    /**
     * retrieves the latest record of {@link com.wks.weather.models.WeatherInfo WeatherInfo} for given city name from the database.
     * @param aCityName name of city.
     * @return instance of {@link com.wks.weather.models.WeatherInfo WeatherInfo} or <code>null</code> if no WeatherInfo for given city is stored in the database.
     */
    public abstract WeatherInfo getWeatherInfo(String aCityName);
    
    /**
     * retrieves today's {@link com.wks.weather.models.WeatherInfo WeatherInfo} for the given city from database, if available.
     * @param aCityName name of city.
     * @return instance of {@link com.wks.weather.models.WeatherInfo WeatherInfo} if today's WeatherInfo is available, otehrwise null.
     */
    public abstract WeatherInfo getCurrentWeather(String aCityName);
    
    /**
     * persists given instance of {@link com.wks.weather.models.WeatherInfo WeatherInfo}
     * @param aWeatherInfo instance of weatherInfo to persist.
     * @return true if weatherInfo persisted successfully, otherwise false.
     */
    public abstract boolean saveWeather(WeatherInfo aWeatherInfo);
    
}
