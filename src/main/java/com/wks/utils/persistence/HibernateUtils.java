package com.wks.utils.persistence;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class HibernateUtils
{
    private static final SessionFactory sessionFactory;

    static{
	try{
	    Configuration config = new Configuration().configure("hibernate.cfg.xml");
	    ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(config.getProperties())
			.build();
	    sessionFactory = config.buildSessionFactory(serviceRegistry);
	}catch(HibernateException e){
	    System.err.printf("Failed to initialise SessionFactory: %s",e);
	    throw new ExceptionInInitializerError(e);
	}
    }
    
    /**
     * 
     * @return a hibernate transaction session.
     */
    public static Session openSession(){
	return sessionFactory.openSession();
    }
}
