package com.wks.utils.encoding;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.JsonDeserializer;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializerProvider;

public class JsonUtils
{
    private static final ObjectMapper mapper;

    static
    {
	mapper = new ObjectMapper();
	mapper.disable(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES);
    }

    /**
     * 
     * @return a singletonn instance of ObjectMapper whose deserialization configuration
     *         {@link org.codehaus.jackson.map.DeserializationConfig.Feature#FAIL_ON_UNKNOWN_PROPERTIES
     *         FAIL_ON_UNKNOWN_PROPERTIES} is disabled.
     */
    public static ObjectMapper getObjectMapper()
    {
	return mapper;
    }

    /**
     * JSONSerializer that serializes {@link java.util.Date Date} to format: dd-MM-yyyy HH:mm:ss
     * @author Waqqas
     *
     */
    public static class TimestampSerializer extends JsonSerializer<Date>
    {

	@Override
	public void serialize(Date value, JsonGenerator gen, SerializerProvider arg2) throws IOException,
		JsonProcessingException
	{
	    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	    String formattedDate = sdf.format(value);
	    gen.writeString(formattedDate);
	}

    }

    /**
     * JSONSerializer that serializes {@link java.util.Date Date} to format: HH:mm.
     * @author Waqqas
     *
     */
    public static class TimeSerializer extends JsonSerializer<Date>
    {

	@Override
	public void serialize(Date value, JsonGenerator gen, SerializerProvider arg2) throws IOException,
		JsonProcessingException
	{
	    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
	    String formattedTime = sdf.format(value);
	    gen.writeString(formattedTime);
	}

    }

    /**
     * JSONDeserializer that deserializes unix time in seconds to a {@link java.util.Date Date} instance.
     * @author Waqqas
     *
     */
    public static class TimeInSecondsDateDeserializer extends JsonDeserializer<Date>
    {

	@Override
	public Date deserialize(JsonParser jp, DeserializationContext arg1) throws IOException, JsonProcessingException
	{
	    JsonNode node = jp.getCodec().readTree(jp);

	    long timeInSeconds = Long.parseLong(node.asText());
	    long timeInMillis = timeInSeconds * 1000;
	    return new Date(timeInMillis);
	}

    }
}
