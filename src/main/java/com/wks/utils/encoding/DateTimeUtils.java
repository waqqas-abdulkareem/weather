package com.wks.utils.encoding;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateTimeUtils
{
    /**
     * Sets the time to midnight for the given date
     * @param aDate date whose time is set to 0:00:00
     * @return a date whose time is set to 0:00:00.
     * @throws NullPointerException if provided date is null.
     */
    public static Date getMidnight(Date aDate){
	if(aDate == null){
	    throw new NullPointerException("aDate");
	}
	
	Calendar calendar = new GregorianCalendar();
	calendar.setTime(aDate);
	calendar.set(Calendar.HOUR_OF_DAY, 0);
	calendar.set(Calendar.MINUTE, 0);
	calendar.set(Calendar.SECOND, 0);
	calendar.set(Calendar.MILLISECOND, 0);
	
	return new Date(calendar.getTimeInMillis());
    }
    
    /**
     * Adds given number of days to a date.
     * @param aDate date to which a number of days are to be added.
     * @param numDays number of days to add.
     * @return a date whose date is numDays in the future from given date.
     * @throws NullPointerException if date is null.
     */
    public static Date addDays(Date aDate,int numDays){
	if(aDate == null){
	    throw new NullPointerException("aDate");
	}
	
	Calendar calendar = new GregorianCalendar();
	calendar.setTime(aDate);
	calendar.add(Calendar.DATE, numDays);
	return new Date(calendar.getTimeInMillis());
    }
    
}
